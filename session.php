<?php
session_start();
if(empty($_SESSION["name"])) {
    unset($_SESSION);
    header("Location: login.php?message=Session Expired!&success=false");
}
?>