<?php
    ob_start();
    session_start();
    include("library/classes/DbManager.php");
    include("library/instamojo/instamojo.php");
    include("library/classes/Common.php");
    
    $config = Config::getConfig();
    $instamojo = new Instamojo\Instamojo($config['instamojo']['apiKey'], $config['instamojo']['authToken']);
    $dbManager = new DbManager();
    $dbManager->connect();
    try {
        $response = $instamojo->paymentDetail($_REQUEST['payment_id']);
        if($response['status'] == 'Credit') {
            $table = 'payment';
            $parameters[0] = array('paymentid' => $response['payment_id'], 'status' => $response['status']);
            $dbManager->insert($table, $parameters);
            $table = 'user';
            $parameters[0] = $_SESSION['registeredUser'];
            $common = new Common();
            $password = $common->generatePassword();

            $encrypted = crypt($password, $config['passwordSalt']);
            $parameters[0]['password'] = $encrypted;
            unset($parameters[0]['register']);
            $dbManager->insert($table, $parameters);
            
            //Send notification email to the customer
            $to = $_SESSION['registeredUser']['email'];
            $message = file_get_contents("library/mailformat/notify-customer.htm");
            $message = str_replace('{customer}', $_SESSION['registeredUser']['name'], $message);
            $message = str_replace('{username}', $_SESSION['registeredUser']['email'], $message);
            $message = str_replace('{password}', $password, $message);
            
            $headers = "FROM: ".$config['emails']['noReply'];
            $subject = "Account Details - ISETOFSA";
            mail($to, $subject, $message, $headers);
            
            //Send notification email to the administrator
            $to = $config['emails']['adminNotification'];
            $message = file_get_contents("library/mailformat/notify-admin.htm");
            $message = str_replace('{name}', $_SESSION['registeredUser']['name'], $message);
            $message = str_replace('{email}', $_SESSION['registeredUser']['email'], $message);
            $message = str_replace('{number}', $_SESSION['registeredUser']['number'], $message);
            $message = str_replace('{address}', $_SESSION['registeredUser']['address'], $message);
            $subject = "New User Notification";
            $headers = "FROM: ".$config['emails']['noReply'];
            mail($to, $subject, $message, $headers);
            
            header("Location: login.php?message=Registration Successful. Please check your email for login details.&success=true");
        } else {
            header("Location: login.php?message=Registration Failed.&success=false");
        }
    }
    catch (Exception $e) {
        print('Error: ' . $e->getMessage());
    }
?>