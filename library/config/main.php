<?php
    class Config {
        public function getConfig() {
            return $config =  [
                'db' =>[
                    'serverName' => "localhost",
                    'username' => "root",
                    'password' => "admin",
                    'dbname' => "ISETOFSA"
                ],
                'instamojo' => [
                    'apiKey' => '',
                    'authToken' => '',
                    'salt' => '',
                    'paymentUrl' => ''
                ],
                'passwordSalt' => '3EbNDS8bdbM', //To encrypt admin and customer passwords
                'emails' => [
                    'noReply' => 'jitheshborgia@gmail.com', //To set from address while sending emails
                    'adminNotification' => 'jitheshborgia@gmail.com' //User registration notification email
                ]
            ];
        }
    }
    
?>