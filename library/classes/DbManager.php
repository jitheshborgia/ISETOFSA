<?php
include(dirname(dirname(__FILE__))."/config/main.php");
class DbManager {
    private $serverName, $username, $password, $dbname, $connection;
    function __construct() {
        $config = Config::getConfig();
        $this->serverName = $config['db']['serverName'];
        $this->username = $config['db']['username'];
        $this->password = $config['db']['password'];
        $this->dbname = $config['db']['dbname'];
    }
    
    public function connect() {
        $this->connection = new mysqli($this->serverName, $this->username, $this->password, $this->dbname);
        if ($this->connection->connect_error) {
            die("Connection failed: " . $this->connection->connect_error);
        }
    }
    
    public function insert($tableName, $parameters) {        
        foreach($parameters as $params) {
            $fieldParams = array();
            $fieldValues = array();
            $parameterAssume = array();
            foreach($params as $param=>$value) {
                $fieldParams[] = $param;
                $fieldValues[] = '\''.$value.'\'';
            }
            
            $fieldParams = implode(',', $fieldParams);
            $fieldValues = implode(',', $fieldValues);
            $statement = $this->connection->prepare("INSERT INTO $tableName ($fieldParams) VALUES ($fieldValues)");

            $statement->execute();
            $statement->close();
        }
    }
    
    public function close() {
        if($this->connection) {
            $this->connection->close();
        }
    }
    
    public function find($tableName, $fields = '*', $condition = '1=1') {
        $query = "SELECT $fields FROM $tableName WHERE $condition"; 
        $statement = $this->connection->query($query); 
        
        foreach($statement as $row) {
            $return[] = $row;
        }
        $statement->close();
        return $return;
    }
    
    public function delete($tableName, $condition) {
        $statement = $this->connection->prepare("DELETE FROM $tableName WHERE $condition");
        $statement->execute();
        $statement->close();
    }
}

?>