<?php 
ob_start();
session_start();

include("library/classes/DbManager.php");
include("library/instamojo/instamojo.php");

$dbManager = new DbManager();
$dbManager->connect();
$config = Config::getConfig();

if(isset($_POST["register"])) {
    $redirectUrl = $config['instamojo']['paymentUrl'];
    $_SESSION['registeredUser'] = $_POST;
    header("Location: $redirectUrl");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>ISETOFSA Register</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Avant">
        <meta name="author" content="The Red Team">

        <!-- <link href="assets/less/styles.less" rel="stylesheet/less" media="all"> -->
        <link rel="stylesheet" href="assets/css/styles.min.css?=113">
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<!-- <script type="text/javascript" src="assets/js/less.js"></script> -->
    </head><body class="focusedform">

        <div class="verticalcenter">
            <a href="index.htm"><img src="assets/img/logo-big.png" alt="Logo" class="brand" /></a>
            <div class="panel panel-primary">
                <form action="" method="POST" class="form-horizontal" style="margin-bottom: 0px !important;">
                        <div class="panel-body">
                            <h4 class="text-center" style="margin-bottom: 25px;">Sign Up</h4>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                        <input type="text" class="form-control" name="number" id="number" placeholder="Contact Number" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input type="text" class="form-control" name="relationName" id="relationName" placeholder="Father's/Husband's Name" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control" name="dateOfBirth" id="datepicker-dateOfBirth" placeholder="Date of Birth" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <input type="text" class="form-control" name="placeOfBirth" id="placeOfBirth" placeholder="Place of Birth" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-flag-o"></i></span>
                                        <select name="nationality" id="nationality" class="form-control" required>
                                            <option value="">--Nationality--</option>
                                            <option>Indian</option>
                                            <option>Other</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <span id="nationality-other">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                            <input type="text" class="form-control" name="passportNumber" id="passportNumber" placeholder="Passport Number">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control" name="dateOfIssue" id="datepicker-dateOfIssue" placeholder="Date of Issue">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                            <input type="text" class="form-control" name="placeOfIssue" id="placeOfIssue" placeholder="Place of Issue">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control" name="expiryDate" id="expiryDate" placeholder="Expiry Date">
                                        </div>
                                    </div>
                                </div>
                            </span>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                        <textarea class="form-control" name="address" id="address" placeholder="Address" required></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="pull-left">
                                <a href="login.php" class="btn btn-default">Cancel</a>
                            </div>
                            <div class="pull-right">
                                <input type="submit" name="register" class="btn btn-success" value="Register" />
                            </div>
                        </div>
                    </form>
            </div>
        </div>

    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    
    <script>
        $(document).ready(function(){
            $('#nationality-other').hide();
            
            $('#nationality').change(function(){
                if($(this).val() == 'Other') {
                    $('#nationality-other').slideDown();
                    $('#nationality-other input:text').prop('required',true);
                } else {
                    $('#nationality-other').slideUp();
                    $('#nationality-other input:text').prop('required',false);
                }
            });
            
            $(function() {   
                $("input[id^='datepicker']").datepicker({
                    changeMonth:true,
                    changeYear:true,
                    yearRange:"-100:+0",
                    dateFormat:"yy-mm-dd" 
                });
                
                $("#expiryDate").datepicker({
                    changeMonth:true,
                    changeYear:true,
                    yearRange:"-0:+50",
                    dateFormat:"yy-mm-dd" 
                });
            });
                
                
        });
                
        
        
    </script>
</html>