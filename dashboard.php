<?php
ob_start();
include("session.php");
include('header.php');
include('menu.php');
include("library/classes/DbManager.php");
$dbManager = new DbManager();
$dbManager->connect();

$message = !empty($_GET["message"]) ? $_GET["message"] : '';
$success = !empty($_GET["success"]) ? $_GET["success"] : false;
if($success == 'true') {
    $class = 'alert-success';
} else {
    $class = 'alert-danger';
}

$tableName = 'file_upload';

$action = !empty($_GET["action"]) ? $_GET["action"] : '';
if($action == "delete") {
    $condition = "userid=".$_SESSION['id']." AND id=".$_GET["id"]." AND alias='".$_GET["alias"]."'";
    $dbManager->delete($tableName, $condition);
    header("Location: dashboard.php?message=File Deleted Successfully!!&success=true");
}

$condition = 'userid='.$_SESSION['id'];
$uploadedFiles = $dbManager->find($tableName, "*", $condition);
if (isset($_POST["upload"])) {
    for ($i = 0; $i < count($_FILES['file']["name"]); $i++) {
        $name = $_FILES['file']["name"][$i];
        $extenstion = explode(".", $name);
        $extenstion = $extenstion[count($extenstion)-1];
        $alias = time().rand(100,122222).'.'.$extenstion;
        $target = 'uploads/'.$alias;
        
        if (move_uploaded_file($_FILES['file']["tmp_name"][$i], $target)) {
            $parameters[$i] = array(
                'userid' => $_SESSION['id'],
                'fileName' => $name,
                'alias' => $alias
            );
        }else {
            header("Location: dashboard.php?message=Failed to Upload File!!&success=false");
        }
    }
    $dbManager->insert($tableName, $parameters);
    header("Location: dashboard.php?message=File(s) Uploaded Successfully!!&success=true");
}
?>
<link rel='stylesheet' type='text/css' href='assets/plugins/jquery-fileupload/css/jquery.fileupload-ui.css' /> 
<link rel='stylesheet' type='text/css' href='assets/plugins/codeprettifier/prettify.css' /> 
<link rel='stylesheet' type='text/css' href='assets/plugins/form-toggle/toggles.css' /> 
<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="dashboard.php">Dashboard</a></li>
            </ol>
            <h1>Files</h1>
        </div>
        <div class="container">
            <?php if(!empty($message)) { ?>
                <div class="alert alert-dismissable <?php echo $class; ?>">
                    <?php echo $message; ?>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                </div>
            <?php } ?>
            <div class="row">  
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4>Uploaded Files</h4>
                            <div class="options"></div>
                        </div>
                        <div class="panel-body">
                            <div id="example_wrapper" class="dataTables_wrapper" role="grid">
                                <div class="row">
                                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables dataTable" id="example" aria-describedby="example_info">
                                        <thead>
                                            <th>Filename</th>
                                            <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <?php for($i=0; $i<count($uploadedFiles); $i++) { ?>
                                            <tr>
                                                <td><a href="uploads/<?php echo $uploadedFiles[$i]['alias']; ?>" target="_blank"><?php echo $uploadedFiles[$i]['fileName']; ?></a></td>
                                                <td><a href="dashboard.php?action=delete&id=<?php echo $uploadedFiles[$i]['id']; ?>&alias=<?php echo $uploadedFiles[$i]['alias']; ?>" onclick="return confirm('Do you really want to delete the file?')">Delete</a></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4>Upload File(s)</h4>
                            <div class="options"></div>
                        </div>
                        <div class="panel-body">
                            <div id="example_wrapper" class="dataTables_wrapper" role="grid">
                                <form method="POST" action="" enctype="multipart/form-data" >
                                    <div class="form-group" id="upload-0">
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-9">
                                            <div class="fileinput fileinput-new" data-provides="fileinput"><input type="hidden" value="" name="...">
                                                <div class="input-group">
                                                    <div class="form-control uneditable-input" data-trigger="fileinput">
                                                        <i class="fa fa-file fileinput-exists"></i>&nbsp;<span class="fileinput-filename"></span>
                                                    </div>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="file[]" accept="pdf/doc/docx/rtf">
                                                    </span>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-9">
                                            <div class="fileinput fileinput-new" data-provides="fileinput"><input type="hidden" value="" name="...">
                                                <div class="input-group">
                                                    <a href="#" id="add-more">Add more</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-9">
                                            <div class="fileinput fileinput-new" data-provides="fileinput"><input type="hidden" value="" name="...">
                                                <div class="input-group">
                                                    <input type="submit" name="upload" class="btn btn-success" value="Upload" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
    </div>
    <!--wrap -->
</div>
<!-- page-content -->
<?php
include('footer.php');
?> 
<script type='text/javascript' src='assets/plugins/form-jasnyupload/fileinput.min.js'></script> 
<script>
    $('#add-more').click(function(){
        var div = $('div[id^="upload"]:last');
        var num = parseInt( div.prop("id").match(/\d+/g), 10 ) +1;
        var clone = div.clone().prop('id', 'upload-'+num );
        $('#upload-'+parseInt( div.prop("id").match(/\d+/g), 10 )).append(clone);
        return false;
    });
</script>