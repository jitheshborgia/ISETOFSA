<?php 
    ob_start();
    session_start();
    include("../library/classes/DbManager.php");
    
    $config = Config::getConfig();
    
    $message = !empty($_GET["message"]) ? $_GET["message"] : '';
    $success = !empty($_GET["success"]) ? $_GET["success"] : false;
    if($success == 'true') {
        $class = 'alert-success';
    } else {
        $class = 'alert-danger';
    }
    
    if(isset($_POST['login'])) {
        $dbManager = new DbManager();
        $dbManager->connect();
        $username = $_POST['username'];
        $password = $_POST['password'];
        $encrypted = crypt($password, $config['passwordSalt']);
        $condition = "email = '$username' AND password = '$encrypted'";
        $user = $dbManager->find('admin_user', '*', $condition);
        
        if(count($user) > 0) {
            $_SESSION['email'] = $user[0]['email'];
            $_SESSION['name'] = $user[0]['name'];
            $_SESSION['id'] = $user[0]['id'];
            header("Location: dashboard.php");
        } else {
            header("Location: login.php?message=Incorrect username or password!&success=false");
        }
        exit;
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>ISETOFSA Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Avant">
        <meta name="author" content="The Red Team">

        <!-- <link href="assets/less/styles.less" rel="stylesheet/less" media="all"> -->
        <link rel="stylesheet" href="../assets/css/styles.min.css?=113">
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>

<!-- <script type="text/javascript" src="assets/js/less.js"></script> -->
    </head><body class="focusedform">

        <div class="verticalcenter">
            <a href="index.htm"><img src="../assets/img/logo-big.png" alt="Logo" class="brand" /></a>
            <div class="panel panel-primary">
                <form action="" method="POST" class="form-horizontal" style="margin-bottom: 0px !important;">
                    <div class="panel-body">
                        <?php if(!empty($message)) { ?>
                            <div class="alert alert-dismissable <?php echo $class; ?>">
                                <?php echo $message; ?>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                            </div>
                        <?php } ?>
                        <h4 class="text-center" style="margin-bottom: 25px;">Log in to get started</h4>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" class="form-control" name="username" id="username" placeholder="Username">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="pull-right"><label><input type="checkbox" style="margin-bottom: 20px" checked=""> Remember Me</label></div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="extras-forgotpassword.htm" class="pull-left btn btn-link" style="padding-left:0">Forgot password?</a>

                        <div class="pull-right">
                            <a href="#" class="btn btn-default">Reset</a>
                            <input type="submit" name="login" value="Login" class="btn btn-primary" />
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </body>
</html>