<?php
ob_start();
include("session.php");
include('header.php');
include('menu.php');
include("../library/classes/DbManager.php");
$dbManager = new DbManager();
$dbManager->connect();

$message = !empty($_GET["message"]) ? $_GET["message"] : '';
$success = !empty($_GET["success"]) ? $_GET["success"] : false;
if ($success == 'true') {
    $class = 'alert-success';
} else {
    $class = 'alert-danger';
}

$user = $dbManager->find('user', '*', 'id=' . $_GET['id']);

$condition = 'userid='.$_GET['id'];
$uploadedFiles = $dbManager->find('file_upload', "*", $condition);

$action = !empty($_GET["action"]) ? $_GET["action"] : '';
if($action == "delete") {
    $condition = "userid=".$_GET["id"]." AND id=".$_GET["fileid"]." AND alias='".$_GET["alias"]."'";
    $dbManager->delete('file_upload', $condition);
    header("Location: view_user.php?message=File Deleted Successfully!!&success=true&id=".$_GET["id"]);
}
?>
<link rel='stylesheet' type='text/css' href='../assets/plugins/jquery-fileupload/css/jquery.fileupload-ui.css' /> 
<link rel='stylesheet' type='text/css' href='../assets/plugins/codeprettifier/prettify.css' /> 
<link rel='stylesheet' type='text/css' href='../assets/plugins/form-toggle/toggles.css' /> 
<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="dashboard.php">Dashboard</a></li>
                <li><a href="view_user.php?id=<?php echo $_GET["id"]; ?>">Users</a></li>
            </ol>
            <h1><?php echo $user[0]["name"]; ?></h1>
        </div>
        <div class="container">
            <?php if (!empty($message)) { ?>
                <div class="alert alert-dismissable <?php echo $class; ?>">
                    <?php echo $message; ?>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                </div>
            <?php } ?>
            <div class="row">  
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4><?php echo $user[0]["name"]; ?></h4>
                            <div class="options"></div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="table-responsive">
                                        <table class="table table-condensed">
                                            <h3><strong><?php echo $user[0]["name"]; ?></strong></h3>
                                            <tbody>
                                                <tr>
                                                    <td>Email</td>
                                                    <td><a href="#"><?php echo $user[0]["email"]; ?></a></td>
                                                </tr>

                                                <tr>
                                                    <td>Phone</td>
                                                    <td><?php echo $user[0]["number"]; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Father's/Husband's Name</td>
                                                    <td><?php echo $user[0]["relationName"]; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Date of Birth</td>
                                                    <td><?php echo $user[0]["dateOfBirth"]; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Place of Birth</td>
                                                    <td><?php echo $user[0]["placeOfBirth"]; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Nationality</td>
                                                    <td><?php echo $user[0]["nationality"]; ?></td>
                                                </tr>
                                                <?php if ($user[0]["nationality"] == "Other") { ?>
                                                    <tr>
                                                        <td>Passport No.</td>
                                                        <td><?php echo $user[0]["passportNumber"]; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Date of Issue</td>
                                                        <td><?php echo $user[0]["dateOfIssue"]; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Place of Issue</td>
                                                        <td><?php echo $user[0]["placeOfIssue"]; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Passport Expiry</td>
                                                        <td><?php echo $user[0]["expiryDate"]; ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h3>Address</h3>
                                    <?php echo $user[0]["address"]; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4>Uploaded Files</h4>
                            <div class="options"></div>
                        </div>
                        <div class="panel-body">
                            <div id="example_wrapper" class="dataTables_wrapper" role="grid">
                                <div class="row">
                                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables dataTable" id="example" aria-describedby="example_info">
                                        <thead>
                                            <th>Filename</th>
                                            <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <?php for($i=0; $i<count($uploadedFiles); $i++) { ?>
                                            <tr>
                                                <td><a href="../uploads/<?php echo $uploadedFiles[$i]['alias']; ?>" target="_blank"><?php echo $uploadedFiles[$i]['fileName']; ?></a></td>
                                                <td><a href="view_user.php?action=delete&fileid=<?php echo $uploadedFiles[$i]['id']; ?>&alias=<?php echo $uploadedFiles[$i]['alias']; ?>&id=<?php echo $_GET["id"] ?>" onclick="return confirm('Do you really want to delete the file?')">Delete</a></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
    </div>
    <!--wrap -->
</div>
<!-- page-content -->
<?php
include('footer.php');
?>