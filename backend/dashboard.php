<?php
ob_start();
include("session.php");
include('header.php');
include('menu.php');
include("../library/classes/DbManager.php");
$dbManager = new DbManager();
$dbManager->connect();

$message = !empty($_GET["message"]) ? $_GET["message"] : '';
$success = !empty($_GET["success"]) ? $_GET["success"] : false;
if($success == 'true') {
    $class = 'alert-success';
} else {
    $class = 'alert-danger';
}

$users = $dbManager->find('user');

?>
<link rel='stylesheet' type='text/css' href='../assets/plugins/jquery-fileupload/css/jquery.fileupload-ui.css' /> 
<link rel='stylesheet' type='text/css' href='../assets/plugins/codeprettifier/prettify.css' /> 
<link rel='stylesheet' type='text/css' href='../assets/plugins/form-toggle/toggles.css' /> 
<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="dashboard.php">Dashboard</a></li>
            </ol>
            <h1>Users</h1>
        </div>
        <div class="container">
            <?php if(!empty($message)) { ?>
                <div class="alert alert-dismissable <?php echo $class; ?>">
                    <?php echo $message; ?>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                </div>
            <?php } ?>
            <div class="row">  
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4>Users</h4>
                            <div class="options"></div>
                        </div>
                        <div class="panel-body">
                            <div id="example_wrapper" class="dataTables_wrapper" role="grid">
                                <div class="row">
                                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables dataTable" id="example" aria-describedby="example_info">
                                        <thead>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Nationality</th>
                                            <th>Actions</th>
                                        </thead>
                                        <tbody>
                                            <?php for($i=0; $i<count($users); $i++) { ?>
                                            <tr>
                                                <td><?php echo $users[$i]["name"]; ?></td>
                                                <td><?php echo $users[$i]["email"]; ?></td>
                                                <td><?php echo $users[$i]["nationality"]; ?></td>
                                                <td><a href="view_user.php?id=<?php echo $users[$i]['id']; ?>">View</a></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
    </div>
    <!--wrap -->
</div>
<!-- page-content -->
<?php
include('footer.php');
?>