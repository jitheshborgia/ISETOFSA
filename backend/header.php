<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>ISETOFSA</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="ISETOFSA">
        <meta name="author" content="The Red Team">

        <!-- <link href="assets/less/styles.less" rel="stylesheet/less" media="all">  -->
        <link rel="stylesheet" href="../assets/css/styles.min.css?=113">
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>


        <link href='../assets/demo/variations/default.css' rel='stylesheet' type='text/css' media='all' id='styleswitcher'> 

        <link href='../assets/demo/variations/default.css' rel='stylesheet' type='text/css' media='all' id='headerswitcher'> 

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
        <!--[if lt IE 9]>
        <link rel="stylesheet" href="assets/css/ie8.css">
                <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
        <script type="text/javascript" src="assets/plugins/charts-flot/excanvas.min.js"></script>
        <![endif]-->

        <!-- The following CSS are included as plugins and can be removed if unused-->

        <link rel='stylesheet' type='text/css' href='../assets/plugins/codeprettifier/prettify.css' /> 
        <link rel='stylesheet' type='text/css' href='../assets/plugins/form-toggle/toggles.css' /> 

<!-- <script type="text/javascript" src="assets/js/less.js"></script> -->
    </head>

    <body class=" ">


        <div id="headerbar">

        </div>

        <header class="navbar navbar-inverse navbar-fixed-top" role="banner">
            <a id="leftmenu-trigger" class="tooltips" data-toggle="tooltip" data-placement="right" title="Toggle Sidebar"></a>
            <a id="rightmenu-trigger" class="tooltips" data-toggle="tooltip" data-placement="left" title="Toggle Infobar"></a>

            <div class="navbar-header pull-left">
                <a class="navbar-brand" href="index.htm">ISETOFSA</a>
            </div>

            <ul class="nav navbar-nav pull-right toolbar">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle username" data-toggle="dropdown"><span class="hidden-xs"><?php echo $_SESSION["name"]; ?> <i class="fa fa-caret-down"></i></span><img src="../assets/demo/avatar/dangerfield.png" alt="Dangerfield" /></a>
                    <ul class="dropdown-menu userinfo arrow">
                        <li class="username">
                            <a href="#">
                                <div class="pull-left"><img class="userimg" src="../assets/demo/avatar/dangerfield.png" alt="Jeff Dangerfield"/></div>
                                <div class="pull-right"><h5>Howdy, <?php echo $_SESSION["name"]; ?></h5><small>Logged in as <span><?php echo $_SESSION["email"]; ?></span></small></div>
                            </a>
                        </li>
                        <li class="userlinks">
                            <ul class="dropdown-menu">
                                <li class="divider"></li>
                                <li><a href="logout.php" class="text-right">Sign Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </header>